/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.cmdaa.gs.model

import reactivemongo.api.bson.{BSONDateTime, BSONDocumentHandler, Macros}

/**
 * Basic Document Metadata.
 *
 * @param createdBy username of the entity who created the document
 * @param updatedBy username of the entity who last updated/created the
 *                  document.
 * @param schema    the schema version number
 * @param created   the date the document was created
 * @param updated   the date the document was last updated
 */
case class DocMeta (
  createdBy: String,
  updatedBy: String,

  schema:  Int          = 1,
  created: BSONDateTime = BSONDateTime(System.currentTimeMillis()),
  updated: BSONDateTime = BSONDateTime(System.currentTimeMillis())
)
object DocMeta {
  implicit val handler: BSONDocumentHandler[DocMeta] =
    Macros.handler[DocMeta]

  /** Creates a new DocMeta from the document's creator's username */
  def fromUser(creator: String): DocMeta =
    DocMeta(creator, creator)

  /** Updated DocMeta from old DocMeta and updator's username */
  def updated(meta: DocMeta, updatedBy: String): DocMeta =
    DocMeta (
      meta.createdBy,
      updatedBy,
      meta.schema,
      meta.created,
      BSONDateTime(System.currentTimeMillis())
    )
}
