/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.cmdaa.gs.model

import reactivemongo.api.bson.{
  BSONArray, BSONDateTime, BSONDocumentHandler, BSONObjectID, Macros
}

/**
 * BSON Document "Schema" for CMDAA Log documents.
 *
 * @param meta        Common Document Metadata
 * @param owner       the username of the Entity that own's the Log.
 * @param bucket      the S3-style "bucket" where the original log data is found.
 * @param path        the S3-style "path"   where the original log data is found.
 * @param runs        list of GrokRuns
 * @param deployGroks list of Groks to deploy to the DaemonSet
 * @param dsConfig    the DaemonSet Configuration for the CmdaaLog
 * @param _id         MongoDB-compatible primary key for CMDAA Log Documents
 */
case class CmdaaLog (
  meta:   DocMeta,
  owner:  String,
  bucket: String,
  path:   String,

  filename: Option[String],

  runs:        List[GrokRun]    = List.empty,
  deployGroks: List[String]     = List.empty,
  dsConfig:    Option[DsConfig] = Some(DsConfig()),
  _id:         BSONObjectID     = BSONObjectID.generate()
)
object CmdaaLog {
  /** BSON de/serializer for CmdaaLog */
  implicit val handler: BSONDocumentHandler[CmdaaLog] = Macros.handler
}

/**
 * BSON Document Schema for GrokRuns. Stored as part of CmdaaLog documents.
 * Contains the parameters for the run and the resulting Groks.
 *
 * @param submitted            the datetime of when the log was uploaded
 * @param status               the status of the llWorkflow run
 * @param logLikelihood        the "ll" parameter for the workflow
 * @param cosSimilarity        the "cs" parameter for the workflow
 * @param associatedEsInstance the URL to the ES Instance handling results
 * @param dsDeployed           whether or not DaemonSet is
 * @param _id                  the GrokRun's BSON ID
 * @param groks                the groks resulting from the workflow
 */
case class GrokRun (
  submitted:            BSONDateTime    = BSONDateTime.apply(System.currentTimeMillis()),
  status:               RunStatus       = RunStatus.SubmittingWorkflowStatus,
  logLikelihood:        Double          = 0.06,
  cosSimilarity:        Double          = 0.9,
  associatedEsInstance: Option[String]  = None,
  dsDeployed:           Option[Boolean] = Some(false),
  _id:                  BSONObjectID    = BSONObjectID.generate(),
  groks:                List[Grok]      = List.empty
)
object GrokRun {
  /** BSON de/serializer for GrokRun */
  implicit val handler: BSONDocumentHandler[GrokRun] = Macros.handler
}

/**
 * BSON Document Schema for Groks. Stored as a part of GrokRuns (and perhaps
 * other document types in the future); Groks specify a pattern as well as a
 * list of logs that have matched with the pattern in the latest runs.
 *
 * @param grok   the grok pattern
 * @param logs   logs that have matched this pattern
 */
case class Grok (
  grok: String,
  logs: List[String] = List.empty
)
object Grok {
  /** BSON de/serializer for Grok */
  implicit val handler: BSONDocumentHandler[Grok] = Macros.handler
}

/**
 * The current state of the GrokRun.
 *
 * @param isRunning  whether or not the GrokRun is currently running
 * @param isComplete whether or not the GrokRun has been completed
 * @param isError    whether or not the GrokRun terminated with an Error
 * @param message    the current message from the GrokRun
 */
case class RunStatus (
  isRunning:  Boolean,
  isComplete: Boolean,
  isError:    Boolean,
  message:    String
)
object RunStatus {
  /** BSON de/serializer for RunStatus */
  implicit val handler: BSONDocumentHandler[RunStatus] = Macros.handler

  /** Worflow has been submitted */
  val SubmittingWorkflowStatus: RunStatus = RunStatus (
    isRunning  = false,
    isComplete = false,
    isError    = false,
    "Workflow Submitted"
  )

  /** Workflow has been completed */
  val CompletedStatus: RunStatus = RunStatus (
    isRunning  = false,
    isComplete = true,
    isError    = false,
    "Workflow Completed"
  )
}

case class DsConfig (
  paths:        BSONArray      = BSONArray.empty,
  nodeLbls:     BSONArray      = BSONArray.empty,
  daemonPrefix: Option[String] = None
)
object DsConfig {
  /** BSON de/serializer for DaemonSet Config */
  implicit val handler: BSONDocumentHandler[DsConfig] = Macros.handler
}
