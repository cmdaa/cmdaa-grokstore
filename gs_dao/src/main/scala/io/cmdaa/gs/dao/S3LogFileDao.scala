/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.cmdaa.gs.dao

import akka.actor.ActorSystem
import akka.stream.Materializer
import akka.stream.alpakka.s3.BucketAccess.{AccessDenied, AccessGranted, NotExists}
import akka.stream.alpakka.s3._
import akka.stream.alpakka.s3.scaladsl.S3
import akka.stream.scaladsl.{Sink, Source}
import akka.util.ByteString
import akka.{Done, NotUsed}

import java.util.NoSuchElementException
import javax.inject.{Inject, Singleton}
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Success

@Singleton
class S3LogFileDao @Inject() (
  system: ActorSystem
)(implicit
  materializer: Materializer
) {
  type S3FileSource = (Source[ByteString, NotUsed], ObjectMetadata)
  type S3DownloadSource = Source[Option[S3FileSource], NotUsed]

  /** Workaround for configuration issues with Alpakka. */
  private val s3settings: S3Settings = S3Ext(system).settings

  /** Checks on the status of the given bucket, creating it if needed */
  def initBucket(bucketName: String)
    (implicit ec: ExecutionContext)
  : Future[Done] = S3
    .checkIfBucketExistsSource(bucketName)
    .withAttributes(S3Attributes.settings(s3settings))
    .runForeach {
      case AccessGranted => Future successful Done
      case AccessDenied => Future.failed (
        new IllegalStateException(s"No Access to S3 Bucket! $bucketName")
      )
      case NotExists => S3
        .makeBucketSource(bucketName)
        .withAttributes(S3Attributes.settings(s3settings))
        .runWith(Sink.head)
      case _ => // silly warning elimination
    }

  /**
   * Lists the files in the bucket under the (optional) prefix.
   *
   * @param bucketName the name of the bucket.
   * @param o_prefix   the prefix
   * @return a streaming Source of "files" in the bucket with the prefix.
   */
  def listFiles(bucketName: String, o_prefix: Option[String] = None)
  : Source[ListBucketResultContents, NotUsed] = S3
    .listBucket(bucketName, o_prefix)
    .withAttributes(S3Attributes.settings(s3settings))

  /**
   * Attempts to fetch a S3 File Download Source.
   *
   * @param bucketName bucket the path resides in
   * @param path       path of the file to download
   * @return
   */
  def fetchFile(bucketName: String, path: String)
    (implicit ec: ExecutionContext)
  : Future[Option[FileFetch]] = {
    val s3File: S3DownloadSource = S3
      .download(bucketName, path)
      .withAttributes(S3Attributes.settings(s3settings))

    (s3File.runWith(Sink.head): Future[Option[S3FileSource]])
      .map(_.map(fs => FileFetch.apply(fs._1, fs._2)))
      .recover {
        case _: NoSuchElementException => None
        case t =>
          t.printStackTrace()
          None
      }
  }

  /** Provides a sink for uploading files to S3. */
  def fileSink(bucketName: String, path: String)
  : Sink[ByteString, Future[MultipartUploadResult]] = S3
    .multipartUpload(bucketName, path)
    .withAttributes(S3Attributes.settings(s3settings))

  /** Copies a file from one place to another. */
  def cp(bucketName: String, from: String, to: String)
  : Future[MultipartUploadResult] = S3
    .multipartCopy(bucketName, from, bucketName, to)
    .withAttributes(S3Attributes.settings(s3settings))
    .run()

  /** Removes a file from an S3 Bucket */
  def rm(bucketName: String, path: String)
  : Future[Done] = S3
    .deleteObject(bucketName, path)
    .withAttributes(S3Attributes.settings(s3settings))
    .runWith(Sink.head)

  /** Moves a file from one place to another. Removing it from where it was. */
  def mv(bucketName: String, from: String, to: String)
    (implicit ec: ExecutionContext)
  : Future[MultipartUploadResult] =
    cp(bucketName, from, to) andThen { case Success(_) =>
      rm(bucketName, from)
    }
}

case class FileFetch (
  source: Source[ByteString, NotUsed],
  meta:   ObjectMetadata
)
