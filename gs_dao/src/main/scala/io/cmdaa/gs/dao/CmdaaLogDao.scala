/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.cmdaa.gs.dao

import io.cmdaa.gs.model._
import reactivemongo.api.bson.collection.BSONCollection
import reactivemongo.api.bson.{BSONObjectID, document}
import reactivemongo.api.commands.WriteResult
import reactivemongo.api.indexes.{Index, IndexType}
import reactivemongo.api.{Cursor, ReadPreference}

import javax.inject._
import scala.concurrent.{ExecutionContext, Future}

/**
 * Data Access Object for CmdaaLog.
 *
 * @param mongoComponents MongoDB connectivity components
 */
@Singleton
class CmdaaLogDao @Inject()(
  protected val mongoComponents: MongoComponents
) {
  val colName: String = "logRuns"

  /** Resolves the "logRuns" collection. */
  protected def collection(implicit ec: ExecutionContext)
  : Future[BSONCollection] =
    mongoComponents.database map { _
      .collection[BSONCollection](colName)
    }

  /**
   * Initializes the collection. Meant to be safe to call even if the
   * collection already exists and is configured.
   */
  def initCollection()
    (implicit ex: ExecutionContext)
  : Future[Boolean] =
    collection flatMap { _
      .indexesManager.ensure(Index(
        key = Seq (
          "bucket" -> IndexType.Ascending,
          "path"   -> IndexType.Ascending
        ),
        name = Some("bucket_path_idx"),
        unique     = true,
        background = true,
        sparse     = false
      ))
    }

  /** Inserts a new CmdaaLog from the given model. */
  def insert(cmdaaLog: CmdaaLog)
    (implicit ec: ExecutionContext)
  : Future[WriteResult] =
    collection flatMap { _
      .insert.one(cmdaaLog)
    }

  /** Deletes a CmdaaLog by ID. */
  def delete(id: BSONObjectID)
    (implicit ec: ExecutionContext)
  : Future[WriteResult] =
    collection flatMap { _
      .delete.one(document("_id" -> id))
    }

  /** Sets the LogLocation value for the CmdaaLog specified by ID */
  def setDsConfig (
    logId:    BSONObjectID,
    username: String,
    logLoc:   DsConfig
  )(implicit ec: ExecutionContext)
  : Future[Option[CmdaaLog]] = {
    val qry = document("_id" -> logId)
    val upt = document(
      "$set" -> document(
        "dsConfig"       -> logLoc,
        "meta.updatedBy" -> username
      ),
      "$currentDate" -> document(
        "meta.updated" -> document("$type" -> "date")
      )
    )

    collection flatMap { _
      .findAndUpdate(qry, upt, fetchNewObject = true)
      .map(_.result[CmdaaLog])
    }
  }

  /** Inserts a new GrokRun into CmdaaLog.runs */
  def insertGrokRun (
    logId:    BSONObjectID,
    username: String,
    grokRun:  GrokRun
  )(implicit ec: ExecutionContext)
  : Future[Option[CmdaaLog]] = {
    val qry = document(
      "_id" -> logId,
      "runs" -> document(
        "$not" -> document(
          "$elemMatch" -> document(
            "logLikelihood" -> grokRun.logLikelihood,
            "cosSimilarity" -> grokRun.cosSimilarity
          )
        )
      )
    )
    val upt = document(
      "$push" -> document("runs" -> grokRun),
      "$currentDate" -> document(
        "meta.updated" -> document("$type" -> "date")
      ),
      "$set" -> document("meta.updatedBy" -> username)
    )

    collection flatMap { _
      .findAndUpdate(qry, upt, fetchNewObject = true)
      .map(_.result[CmdaaLog])
    }
  }

  /** Deletes a GrokRun by LogId and RunId */
  def deleteRun (
    logId:    BSONObjectID,
    runId:    BSONObjectID,
    username: String
  )(implicit ec: ExecutionContext)
  : Future[Option[CmdaaLog]] = {
    val qry = document("_id" -> logId)
    val upd = document(
      "$pull" -> document("runs" -> document("_id" -> runId)),
      "$currentDate" -> document(
        "meta.updated" -> document("$type" -> "date")
      ),
      "$set" -> document("meta.updatedBy" -> username)
    )

    collection flatMap { _
      .findAndUpdate(qry, upd, fetchNewObject = true)
      .map(_.result[CmdaaLog])
    }
  }

  /** Update runs.$.status for the log/run */
  def updateRunStatus (
    logId:  BSONObjectID,
    runId:  BSONObjectID,
    status: RunStatus
  )(implicit ec: ExecutionContext)
  : Future[Option[CmdaaLog]] = {
    val qry = document("_id" -> logId, "runs._id" -> runId)
    val upd = document(
      "$set" -> document("runs.$.status" -> status),
      "$currentDate" -> document(
        "meta.updated" -> document("$type" -> "date")
      )
    )

    collection flatMap { _
      .findAndUpdate(qry, upd, fetchNewObject = true)
      .map(_.result[CmdaaLog])
    }
  }

  /** Update "runs.$.associatedEsInstance" for the log/run */
  def updateEsInstance (
    logId: BSONObjectID,
    runId: BSONObjectID,
    esInstance: String
  )(implicit ec: ExecutionContext)
  : Future[Option[CmdaaLog]] = {
    val qry = document("_id" -> logId, "runs._id" -> runId)
    val upd = document(
      "$set" -> document("runs.$.associatedEsInstance" -> esInstance),
      "$currentDate" -> document(
        "meta.updated" -> document("$type" -> "date")
      )
    )

    collection flatMap { _
      .findAndUpdate(qry, upd, fetchNewObject = true)
      .map(_.result[CmdaaLog])
    }
  }

  /** Delete "runs.$.associatedEsInstance" for the log/run id */
  def deleteEsInstance(
    logId: BSONObjectID,
    runId: BSONObjectID
  )(implicit ec: ExecutionContext)
  : Future[Option[CmdaaLog]] = {
    val qry = document("_id" -> logId, "runs._id" -> runId)
    val upd = document(
      "$unset" -> document("runs.$.associatedEsInstance" -> ""),
      "$currentDate" -> document(
        "meta.updated" -> document("$type" -> "date")
      )
    )

    collection flatMap { _
      .findAndUpdate(qry, upd, fetchNewObject = true)
      .map(_.result[CmdaaLog])
    }
  }

  /** Update runs.$.dsDeployed for the log/run id */
  def updateDaemonSetDeployed (
    logId: BSONObjectID,
    runId: BSONObjectID,
    deployed_? : Boolean
  )(implicit ec: ExecutionContext)
  : Future[Option[CmdaaLog]] = {
    val qry = document("_id" -> logId, "runs._id" -> runId)
    val upd = document(
      "$set" -> document("runs.$.dsDeployed" -> deployed_?),
      "$currentDate" -> document(
        "meta.updated" -> document("$type" -> "date")
      )
    )

    collection flatMap { _
      .findAndUpdate(qry, upd, fetchNewObject = true)
      .map(_.result[CmdaaLog])
    }
  }

  /** Update runs.$.groks.$.grok */
  def updateGrok (
    logId:  BSONObjectID,
    runId:  BSONObjectID,
    grkIdx: Int,
    grkStr: String
  )(implicit ec: ExecutionContext)
  : Future[Option[CmdaaLog]] = {
    val qry = document("_id" -> logId, "runs._id" -> runId)
    val upd = document(
      "$set" -> document("runs.$.groks."+grkIdx+".grok" -> grkStr)
    )

    collection flatMap { _
      .findAndUpdate(qry, upd, fetchNewObject = true)
      .map(_.result[CmdaaLog])
    }
  }

  /** Set runs.$.groks */
  def setGroks (
    logId: BSONObjectID,
    runId: BSONObjectID,
    groks: List[Grok]
  )(implicit ec: ExecutionContext)
  : Future[Option[CmdaaLog]] = {
    val qry = document("_id" -> logId, "runs._id" -> runId)
    val upd = document("$set" -> document("runs.$.groks" -> groks))

    collection flatMap { _
      .findAndUpdate(qry, upd, fetchNewObject = true)
      .map(_.result[CmdaaLog])
    }
  }

  /** Adds Groks to a run's "groks" Set */
  def addToGrokSet (
    logId: BSONObjectID,
    runId: BSONObjectID,
    groks: List[Grok]
  )(implicit ec: ExecutionContext)
  : Future[Option[CmdaaLog]] = {
    val qry = document("_id" -> logId, "runs._id" -> runId)
    val upd = document(
      "$addToSet" -> document(
        "runs.$.groks" -> document("$each" -> groks)
      )
    )

    collection flatMap { _
      .findAndUpdate(qry, upd, fetchNewObject = true)
      .map(_.result[CmdaaLog])
    }
  }

  /** Insert or update a particular CmdaaLog */
  def upsert(cmdaaLog: CmdaaLog)
    (implicit ec: ExecutionContext)
  : Future[WriteResult] = {
    val selector = document(
      "bucket" -> cmdaaLog.bucket,
      "path"   -> cmdaaLog.path
    )

    collection flatMap { _
      .update.one(selector, cmdaaLog, upsert = true)
    }
  }

  /** Update a particular CmdaaLog */
  def update(cmdaaLog: CmdaaLog)
    (implicit ec: ExecutionContext)
  : Future[WriteResult] = {
    collection flatMap { _
      .update.one(document("_id" -> cmdaaLog._id), cmdaaLog)
    }
  }

  /** Find a CmdaaLog by ID */
  def findByLogId(id: BSONObjectID)
    (implicit ec: ExecutionContext)
  : Future[Option[CmdaaLog]] =
    collection flatMap { _
      .find(document("_id" -> id ))
      .one[CmdaaLog]
    }

  /** Find a CmdaaLog by stringified BSON ID */
  def findByLogId(id: String)
    (implicit ec: ExecutionContext)
  : Future[Option[CmdaaLog]] =
    findByLogId(BSONObjectID.parse(id).get)

  /**
   * Find a CmdaaLog by bucket and path. Bucket and Path are assumed to be
   * "natural keys" for CmdaaLogs, so this returns at most 1.
   *
   * @param bucket the S3-style bucket name
   * @param path   the S3-style path
   * @param ec     the implicit ExecutionContext
   * @return the CmdaaLog (if it exists)
   */
  def findByBucketPath(bucket: String, path: String)
    (implicit ec: ExecutionContext)
  : Future[Option[CmdaaLog]] =
    collection flatMap { _
      .find(document("bucket" -> bucket, "path" -> path))
      .one[CmdaaLog]
    }

  /**
   * Finds CmdaaLogs by bucket name. Assumes (for now) that we can fetch all of
   * the documents into a List (for convenience sake). We may want to replace
   * this with "Cursor" or streaming response later.
   *
   * @param bucket the S3-style bucket name
   * @param ec     the implicit ExecutionContext
   * @return the CmdaaLogs for the given bucket
   */
  def findByBucket(bucket: String)
    (implicit ec: ExecutionContext)
  : Future[List[CmdaaLog]] =
    collection flatMap { _
      .find(document("bucket" -> bucket))
      .cursor[CmdaaLog](ReadPreference.primary)
      .collect[List](-1, Cursor.FailOnError[List[CmdaaLog]]())
    }

  /**
   * Finds CmdaaLogs by bucket name. Assumes (for now) that we can fetch all of
   * the documents into a List (for convenience sake). We may want to replace
   * this with "Cursor" or a streaming response later.
   *
   * @param owner the username of the owner of the document. When we redo
   *              authentication change this to BSON ID or something useful.
   * @param ec    the implicit ExecutionContext
   *
   * @return the CmdaaLogs for the given owner
   */
  def findByOwner(owner: String)
    (implicit ec: ExecutionContext)
  : Future[List[CmdaaLog]] =
    collection flatMap { _
      .find(document("owner" -> owner))
      .cursor[CmdaaLog](ReadPreference.primary)
      .collect[List](-1, Cursor.FailOnError[List[CmdaaLog]]())
    }
}
