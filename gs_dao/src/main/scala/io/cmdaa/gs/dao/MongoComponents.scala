/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.cmdaa.gs.dao

import reactivemongo.api.{AsyncDriver, DB, MongoConnection}

import scala.concurrent.{ExecutionContext, Future}

/**
 * Meant to resemble ReactiveMongoApi from "play2-reactivemongo". Hopefully
 * making the DAOs useful in both Play and non-Play environments.
 */
trait MongoComponents {
  /** Provisioned ReactiveMongo driver */
  def asyncDriver: AsyncDriver

  /** Default ReactiveMongo pool for the application */
  def connection: MongoConnection

  /** Default database reference for the application */
  def database: Future[DB]
}

/**
 * Default Implementation of MongoComponents. See the "fromUri" method in the
 * companion Object for easy creation.
 *
 * @param asyncDriver Akka Actor container
 * @param connection  MongoDB Connection Pool
 * @param database    Reference to a MongoDB Database
 */
case class DefaultMongoComponents (
  asyncDriver: AsyncDriver,
  connection:  MongoConnection,
  database:    Future[DB]
) extends MongoComponents

object DefaultMongoComponents {
  /**
   * Constructs a Future MongoComponents for use in Applications.
   *
   * @param mongoUri the full Mongo Connection URL
   * @param ec       ExecutionContext for ReactiveMongo
   * @return asynchronous MongoComponents
   */
  def fromUri(mongoUri: String)
    (implicit ec: ExecutionContext)
  : Future[DefaultMongoComponents] = {
    val driver = new AsyncDriver

    for {
      uri <- MongoConnection.fromString(mongoUri)
      con <- driver.connect(uri)
    } yield DefaultMongoComponents(driver, con, con.database(uri.db.get))
  }
}
