#!/bin/bash

TAG=0.0.23

sbt clean dist
docker build -t cmdaa/cmdaa-grokstore:${TAG} .
