// ScalaJS 1.7.0
addSbtPlugin("org.scala-js"       % "sbt-scalajs"              % "1.7.0")
addSbtPlugin("ch.epfl.scala"      % "sbt-scalajs-bundler"      % "0.20.0")
addSbtPlugin("org.portable-scala" % "sbt-scalajs-crossproject" % "1.0.0")

// Play 2.8.8
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.8.8")

// ScalaFix for enabling cross-compiling
addSbtPlugin("ch.epfl.scala" % "sbt-scalafix" % "0.9.27")
