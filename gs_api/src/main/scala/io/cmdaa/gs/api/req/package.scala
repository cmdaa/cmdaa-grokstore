/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.cmdaa.gs.api

import play.api.libs.json.{Format, Json}

package object req {

  /**
   * Wrapper for InsertCmdaaLog Requests.
   *
   * @param cmdaaLog the new CmdaaLog to insert
   */
  case class InsertCmdaaLog(cmdaaLog: NewCmdaaLog)
  object InsertCmdaaLog {
    implicit val format: Format[InsertCmdaaLog] = Json.format
  }

  /**
   * Required info for creating a new CmdaaLog from a client (such as the UI).
   *
   * @param bucket   the S3 Bucket the log is stored in
   * @param path     the file path of the log in the S3 Bucket
   * @param filename original name of the uploaded file
   */
  case class NewCmdaaLog (
    bucket:   String,
    path:     String,
    filename: String,

    llThreshold:  Double,
    cosThreshold: Double
  )
  object NewCmdaaLog {
    implicit val format: Format[NewCmdaaLog] = Json.format
  }

  /**
   * Request Wrapper for inserting new GrokRuns
   *
   * @param newGrokRun data for the new GrokRun to insert
   */
  case class InsertGrokRun(newGrokRun: NewGrokRun)
  object InsertGrokRun {
    implicit val format: Format[InsertGrokRun] = Json.format
  }

  /** Request wrapper for setting DaemonSet Config */
  case class SetDaemonSetConfig(dsConfig: DsConfig)
  object SetDaemonSetConfig {
    implicit val format: Format[SetDaemonSetConfig] = Json.format
  }

  /** Data for inserting a new GrokRun into an existing CmdaaLog. */
  case class NewGrokRun (
    llThreshold:  Double,
    cosThreshold: Double
  )
  object NewGrokRun {
    implicit val format: Format[NewGrokRun] = Json.format
  }

  /**
   * Required info for updating a CmdaaLog/GrokRun's Status.
   *
   * @param status the RunStatus to set for the CmdaaLog/GrokRun
   */
  case class UpdateLogStatus(status: RunStatus)
  object UpdateLogStatus {
    implicit val format: Format[UpdateLogStatus] = Json.format
  }

  /** Required info for updating a CmdaaLog/GrokRun's dsDeployed state. */
  case class UpdateDaemonSetDeployed(dsDeployed: Boolean)
  object UpdateDaemonSetDeployed {
    implicit val format: Format[UpdateDaemonSetDeployed] = Json.format
  }

  /** Required info for update a Log/Run's "associatedEsInstance" */
  case class UpdateEsInstance(esInstance: String)
  object UpdateEsInstance {
    implicit val format: Format[UpdateEsInstance] = Json.format
  }

  /**
   * Grok update data.
   *
   * @param grok the new, revised GrokString
   */
  case class UpdateGrok(grok: String)
  object UpdateGrok {
    implicit val format: Format[UpdateGrok] = Json.format
  }

  /**
   * Request to Set Groks
   *
   * @param groks the list of Groks to set
   */
  case class SetGroks(groks: List[Grok])
  object SetGroks {
    implicit val format: Format[SetGroks] = Json.format
  }
}
