/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.cmdaa.gs.api

import enumeratum._
import io.cmdaa.gs.api.s3.BucketItem
import play.api.libs.json.{Format, Json}

package object resp {

  /** Service Response for a single CmdaaLog */
  case class FetchCmdaaLog(log: CmdaaLog)
  object FetchCmdaaLog {
    /** JSON de/serializer for FetchCmdaaLog */
    implicit val format: Format[FetchCmdaaLog] = Json.format
  }

  /** A service response with a list of CmdaaLogs. */
  case class FetchLogList(logs: List[CmdaaLog])
  object FetchLogList {
    /** JSON de/serializer for FetchLogList */
    implicit val format: Format[FetchLogList] = Json.format
  }

  /** A service response with a list of BucketItems */
  case class FetchFileList(files: List[BucketItem])
  object FetchFileList {
    /** JSON de/serializer for FetchFileList */
    implicit val format: Format[FetchFileList] = Json.format
  }

  /** Generic S3 Service Response with bucket/path */
  case class S3SvcResp (
    result: SvcResult,
    bucket: String,
    path:   String,
  )
  object S3SvcResp {
    /** JSON de/serializer for S3SvcResp */
    implicit val format: Format[S3SvcResp] = Json.format
  }

  /**
   * Service Results. Some HTTP clients aren't clear on HTTP Status Codes.
   * This makes it easier to throw up simple result output for clarity on those
   * clients.
   */
  sealed trait SvcResult extends EnumEntry
  object SvcResult extends Enum[SvcResult] with PlayJsonEnum[SvcResult] {
    val values = findValues

    case object SvcSuccess  extends SvcResult
    case object SvcNotFound extends SvcResult

    case class SvcFailure(msg: String) extends SvcResult
  }
}
