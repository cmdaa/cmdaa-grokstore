/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.cmdaa.gs.api

import play.api.libs.json.{Format, Json}

/**
 * The API version of what's stored in MongoDB. Primarily used in responses.
 *
 * @param id          the document's ID
 * @param meta        document metadata
 * @param owner       document owner
 * @param bucket      the log's S3 Bucket
 * @param path        the log's path
 * @param filename    original name of the uploaded file
 * @param runs        the log's Grok runs
 * @param deployGroks the Groks to Deploy to a DaemonSet
 * @param dsConfig    the DaemonSet Configuration
 */
case class CmdaaLog (
  id:     String,
  meta:   DocMeta,
  owner:  String,
  bucket: String,
  path:   String,

  filename: Option[String],

  runs:        List[GrokRun],
  deployGroks: List[String],
  dsConfig:    Option[DsConfig]
) {
  import CmdaaLog.pathPattern

  /** Derived PathData for this CmdaaLog */
  lazy val pathData: PathData = derivePathData()

  /**
   * Derives PathData from the path.
   *
   * @return Some PathData for paths that conform the to expected format.
   */
  protected def derivePathData(): PathData = {
    path match {
      case pathPattern(user, logName, lastModifiedStr) =>
        PathData(user, logName, lastModifiedStr.toLong)
      case _ =>
        println(s"""PathPattern not found in: "$path".""" )
        PathData (
          meta.createdBy,
          path.substring(path.lastIndexOf('/')),
          meta.createdBy.toLong
        )
    }
  }
}
object CmdaaLog {
  /** Regex for deriving PathData from the path */
  private val pathPattern =
    """([0-9a-zA-Z.\-_@ ]+)/([0-9a-zA-Z.\-_ ]+)_([0-9]+)/log""".r

  /** JSON de/serialization for CmdaaLog */
  implicit val format: Format[CmdaaLog] = Json.format
}

/**
 * The API version of what's stored in MongoDB. Primarily used in responses.
 *
 * @param id                   the GrokRun ID
 * @param submitted            date the log was uploaded
 * @param status               the status of the workflow/run
 * @param logLikelihood        the "ll" configuration parameter
 * @param cosSimilarity        the "cs" configuration parameter
 * @param associatedEsInstance URL to the ES Instance
 * @param dsDeployed           whether or not DaemonSet is deployed for GrokRun
 * @param groks                grok derived from this run
 */
case class GrokRun (
  id:                   String,
  submitted:            String,
  status:               RunStatus,
  logLikelihood:        Double,
  cosSimilarity:        Double,
  associatedEsInstance: Option[String],
  dsDeployed:           Boolean,
  groks:                List[Grok]
)
object GrokRun {
  /** JSON de/serialization for GrokRun */
  implicit val format: Format[GrokRun] = Json.format
}

/**
 * The API version of what's stored in MongoDB. Primarily used in responses.
 *
 * @param grok   the extracted grok
 * @param logs   examples of logs that correspond with the Grok
 */
case class Grok (
  grok: String,
  logs: List[String]
)
object Grok {
  /** JSON de/serialization for Grok */
  implicit val format: Format[Grok] = Json.format
}

/**
 * The current state of the GrokRun.
 *
 * @param isRunning  whether or not the GrokRun is currently running
 * @param isComplete whether or not the GrokRun has been completed
 * @param isError    whether or not the GrokRun terminated with an Error
 * @param message    the current message from the GrokRun
 */
case class RunStatus (
  isRunning:  Boolean,
  isComplete: Boolean,
  isError:    Boolean,
  message:    String
)
object RunStatus {
  /** JSON de/serialization for RunStaus */
  implicit val format: Format[RunStatus] = Json.format

  /** Worflow has been submitted */
  val SubmittingWorkflowStatus: RunStatus = RunStatus (
    isRunning  = true,
    isComplete = false,
    isError    = false,
    "Workflow Submitted"
  )

  /** Workflow has been completed */
  val CompletedStatus: RunStatus = RunStatus (
    isRunning  = false,
    isComplete = true,
    isError    = false,
    "Workflow Completed"
  )
}

/**
 * LogLocation for deploying "Daemon Sets".
 *
 * @param paths    log file paths
 * @param nodeLbls node labels
 */
case class DsConfig (
  paths:        Array[String],
  nodeLbls:     Array[String],
  daemonPrefix: Option[String]
)
object DsConfig {
  implicit val format: Format[DsConfig] = Json.format
}

/**
 * Data derived from "path" in CmdaaLog.
 *
 * @param owner        username of the uploader
 * @param logName      simplified name of the uploaded log
 * @param lastModified when the log was last modified
 */
case class PathData (
  owner: String,
  logName: String,
  lastModified: Long
)
