import play.sbt.routes.RoutesKeys
import sbt.Keys.version
import sbtcrossproject.CrossPlugin.autoImport.{CrossType, crossProject}

val nexus = "https://52.72.213.202/repository/maven-"

val playVer     = "2.8.8"
val akkaVer     = "2.6.14"
val akkaHttpVer = "10.2.5"

lazy val scala212 = "2.12.12"
lazy val scala213 = "2.13.3"
lazy val supportedScalaVersions = List(scala212, scala213)

ThisBuild / organization := "io.cmdaa.grokstore"
ThisBuild / version      := "0.0.23-SNAPSHOT"
ThisBuild / scalaVersion := scala213

ThisBuild / publishTo := {
  if (isSnapshot.value)
    Some("CMDAA Snapshots Nexus" at nexus+"snapshots")
  else
    Some("CMDAA Releases Nexus" at nexus+"releases")
}

ThisBuild / resolvers ++= Seq (
  Resolver.mavenLocal,
  "CMDAA Nexus" at nexus+"public/"
)

lazy val root = (project in file("."))
  .aggregate(gs_api.jvm, gs_api.js, gs_dao, gs_service)
  .settings(
    name := "cmdaa-gs",
    publish / skip := true,
    crossScalaVersions := Nil
  )

lazy val gs_api = crossProject(JSPlatform, JVMPlatform)
  .crossType(CrossType.Pure)
  .in(file("gs_api"))
  .settings(
    name := "cmdaa-gs-api",
    libraryDependencies ++= Seq(
      "com.typesafe.play" %%% "play-json"            % "2.9.2",
      "com.beachape"      %%% "enumeratum-play-json" % "1.6.1"
    ),
    crossScalaVersions := List(scala212, scala213)
  )

lazy val gs_dao = (project in file("gs_dao"))
  .settings(
    name := "cmdaa-gs-dao",
    ThisBuild / scalafixDependencies += "org.scala-lang.modules" %% "scala-collection-migrations" % "2.4.4",
    libraryDependencies ++= Seq(
      "javax.inject" % "javax.inject" % "1",

      "com.lightbend.akka" %% "akka-stream-alpakka-s3" % "3.0.3",
      "com.typesafe.akka"  %% "akka-stream"            % akkaVer,

      "org.reactivemongo"      %% "reactivemongo"           % "1.0.7",
      "org.scala-lang.modules" %% "scala-collection-compat" % "2.5.0"
    ),
    crossScalaVersions := supportedScalaVersions,
    addCompilerPlugin(scalafixSemanticdb),
    scalacOptions ++= Seq("-Yrangepos", "-P:semanticdb:synthetics:on")
  )

lazy val gs_service = (project in file("gs_service"))
  .enablePlugins(PlayScala)
  .dependsOn(gs_api.jvm, gs_dao)
  .settings(
    name := "cmdaa-gs-service",
    libraryDependencies ++= Seq(
      guice, ws,

      "io.cmdaa.auth" %% "cmdaa-auth-api" % "0.0.23-SNAPSHOT",

      "com.lightbend.akka" %% "akka-stream-alpakka-s3" % "3.0.3",
      "com.typesafe.akka"  %% "akka-stream"            % akkaVer,
      "com.typesafe.akka"  %% "akka-http"              % akkaHttpVer,
      "com.typesafe.akka"  %% "akka-http-xml"          % akkaHttpVer,

      "org.reactivemongo"      %% "play2-reactivemongo" % "1.0.6-play28",
      "org.scalatestplus.play" %% "scalatestplus-play"  % "5.1.0" % "test"
    ),
    publish / skip := true,
    crossScalaVersions := Nil,
    RoutesKeys.routesImport ++= Seq(
      "play.modules.reactivemongo.PathBindables._",
      "reactivemongo.api.bson.BSONObjectID"
    )
  )
