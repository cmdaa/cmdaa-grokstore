CMDAA Grokstore
===============

For development, use `sbt`:

    $ sbt
    [info] welcome to sbt....

    sbt:cmdaa-gs> ~gs_service/run
    ....
    [info] p.c.s.AkkaHttpServer - Listening for HTTP on /0:0:0:0:0:0:0:0:9000
    ....

Putting `~` before a command causes sbt watch for file changes and rebuild as
you edit. Despite what sbt says, when it's performing `~run` in a Play project
you may need to press `Ctrl-D` then Enter to stop the server and go back to the
console.

Common sbt commands include:

 - `clean`:   Deletes the `target/` directory.
 - `compile`: Compiles the code.
 - `run`:     Runs the program.

Doing everything from the console saves time when there are multiple commands
that you need to run. But when you really just want to do one thing, call the
commands like this: `sbt <commands>`.

For instance:

 - `$ sbt clean compile` - Recompile the project.
 - `$ sbt publish`       - Publishes the project to Nexus.
