/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.cmdaa.gs.service.controllers.api01

import akka.stream.Materializer
import akka.stream.alpakka.s3.MultipartUploadResult
import io.cmdaa.auth.api.CmdaaUser
import io.cmdaa.gs.api.resp.SvcResult.{SvcFailure, SvcSuccess}
import io.cmdaa.gs.api.resp.{FetchFileList, S3SvcResp}
import io.cmdaa.gs.api.s3.BucketItem
import io.cmdaa.gs.dao.S3LogFileDao
import io.cmdaa.gs.service.AppConfig
import io.cmdaa.gs.service.actions.AuthAction
import io.cmdaa.gs.service.dao.s3BucketItem2Api
import play.api.libs.json.Json
import play.api.libs.streams.Accumulator
import play.api.mvc.MultipartFormData.FilePart
import play.api.mvc._
import play.api.{Configuration, Logging}
import play.core.parsers.Multipart
import play.core.parsers.Multipart.FileInfo

import java.util.UUID
import javax.inject.{Inject, Singleton}
import scala.collection.mutable.ListBuffer
import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

@Singleton
class S3LogFileController @Inject() (
  cc: ControllerComponents,
  config: Configuration,
  authAction: AuthAction,
  logFileDao: S3LogFileDao
)(implicit ec: ExecutionContext, materializer: Materializer)
  extends AbstractController(cc)
     with Logging
{
  /** Base URL for the AuthSessionService */
  private val s3Bucket = config.get[AppConfig]("app").bucket

  logFileDao.initBucket(s3Bucket).onComplete {
    case _: Success[_] =>
      logger.debug("S3LogFileController started. Configured Bucket: " +
        s"'$s3Bucket' exists, access granted.")
    case Failure(t) =>
      logger.error(s"Access denied writing to S3 Bucket: '$s3Bucket'.", t)
  }

  /** Action for route 'GET /api/v1/files' */
  def list(): Action[AnyContent] =
    authAction.async { authdReq =>
      val user = authdReq.user
      logger.info(s"GET '/files' - User: ${user.username}")

      val path = user.username+"/"

      logFileDao
        .listFiles(s3Bucket, Some(path))
        .map(s3BucketItem2Api)
        .runFold(new ListBuffer[BucketItem]())(_ += _)
        .map(lb => Ok(Json toJson FetchFileList(lb.toList)))
        .recover(svcErrorHandler(path, "Error listing files."))
    }

  /** Action for route 'GET /api/v1/files/file?path=:path' */
  def getFile(path: String): Action[AnyContent] =
    authAction.async { authdReq =>
      val user = authdReq.user
      logger.info(s"GET '/files?filePath=$path' - User: ${user.username}")

      logFileDao
        .fetchFile(s3Bucket, path)
        .map { _.fold(res404(path)) { ffile =>
          logger.info(s"Bucket: $s3Bucket; Path: '$path' - File Downloaded.")

          Ok.streamed (
            ffile.source,
            Some(ffile.meta.contentLength),
            ffile.meta.contentType
          )
        } }
        .recover(svcErrorHandler(path, "Error fetching file."))
    }

  /** Action for route 'PUT /api/v1/files/log */
  def putLog(): Action[MultipartFormData[MultipartUploadResult]] =
    authAction(uploadParser).async { authdReq =>
      val user = authdReq.user
      logger.info(s"PUT '/files/log' - User: ${user.username}")

      val lmDate = authdReq.body.dataParts
        .get("lmDate").flatMap(_.headOption).flatMap(_.toLongOption)
        .getOrElse(System.currentTimeMillis())

      val file = authdReq.body.files.head
      val path = cmdaaLogPath(user, file, lmDate)

      logFileDao
        .mv(s3Bucket, file.ref.key, path)
        .map { _ =>
          logger.info(s"Bucket: $s3Bucket; Path: '$path' - File Uploaded.")
          Ok(Json toJson S3SvcResp(SvcSuccess, s3Bucket, path))
        }
        .recover(svcErrorHandler(path, s"Error moving file from ${file.ref.key}."))
    }

  /** Action for route 'DELETE /api/v1/files/file?path=:path' */
  def deleteFile(path: String): Action[AnyContent] =
    authAction.async { authdReq =>
      val user = authdReq.user
      logger.info(s"DELETE '/files/file?filePath=$path'" +
        s" - User: ${user.username}")

      logFileDao
        .rm(s3Bucket, path)
        .map { _ =>
          logger.info(s"Bucket: $s3Bucket; Path: '$path' - File Deleted.")
          Ok(Json toJson S3SvcResp(SvcSuccess, s3Bucket, path))
        }
        .recover(svcErrorHandler(path, "Error deleting file."))
    }

  /** Body parser for streaming data straight to S3 */
  private def uploadParser
  : BodyParser[MultipartFormData[MultipartUploadResult]] =
    parse.multipartFormData(fpHandler, Long.MaxValue, allowEmptyFiles = false)

  /** FilePart Handler for streaming to S3 */
  private def fpHandler
  : Multipart.FilePartHandler[MultipartUploadResult] = {
    case FileInfo(partName, fileName, contentType, dispositionType) =>
      println(FileInfo(partName, fileName, contentType, dispositionType))

      Accumulator(logFileDao.fileSink(s3Bucket, tmpPath))
        .map { result =>
          FilePart(partName, fileName, contentType, result)
        }
  }

  /** Format for CMDAA Log paths */
  private def cmdaaLogPath (
    user:   CmdaaUser,
    file:   FilePart[MultipartUploadResult],
    lmDate: Long
  ): String = {
    val filename = file.filename.replaceAll(" ", "_")
    s"${user.username}/${filename}_$lmDate/log"
  }

  /** Format for temporary upload paths */
  private def tmpPath: String =
    "tmp/tmp_"+UUID.randomUUID().toString

  /** Generic Response for a document can not be found by ID */
  private def res404(path: String): Result =
    NotFound(Json.obj(
      "status" -> "NotFound",
      "code"   -> 404,
      "bucket" -> s3Bucket,
      "path"   -> path
    ))

  /** Service Error Handler */
  def svcErrorHandler(path: String, logMsg: String)
  : PartialFunction[Throwable, Result] = { case t: Throwable =>
    logger.error(s"Bucket: $s3Bucket; Path: '$path' - $logMsg", t)
    InternalServerError(Json toJson S3SvcResp(
      SvcFailure(t.getMessage), s3Bucket, path))
  }
}
