/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.cmdaa.gs.service.controllers.api01

import io.cmdaa.gs.api.req._
import io.cmdaa.gs.api.resp.{FetchCmdaaLog, FetchLogList}
import io.cmdaa.gs.dao.CmdaaLogDao
import io.cmdaa.gs.service.actions.AuthAction
import io.cmdaa.gs.service.dao._
import io.cmdaa.gs.{model => mongo}
import play.api.Logging
import play.api.libs.json._
import play.api.mvc._
import reactivemongo.api.bson.BSONObjectID

import javax.inject._
import scala.collection.{Seq => scSeq}
import scala.concurrent.{ExecutionContext, Future}

/**
 * Controller for routes concerning CMDAA Logs
 *
 * @param cc         ControllerComponents
 * @param authAction Authenticated Action
 * @param logDao     CmdaaLog Data Access Object
 * @param ec         ExecutionContext for the MongoDB Services
 */
@Singleton
class CmdaaLogController @Inject() (
  cc:         ControllerComponents,
  authAction: AuthAction,
  logDao:     CmdaaLogDao
)(implicit ec: ExecutionContext)
  extends AbstractController(cc)
     with Logging
{
  /** The name of the collection */
  private val colName = logDao.colName

  logDao.initCollection() foreach { newConfigs =>
    if (newConfigs)
      logger.info(s"Collection '$colName': Configured")
    else
      logger.debug(s"Collection '$colName': Exists")
  }

  /** Action for route GET '/api/v1/logs' */
  def list(): Action[AnyContent] =
    authAction.async { authdReq =>
      val user = authdReq.user
      logger.info(s"GET 'logs' - User: ${user.username}")

      logDao.findByOwner(user.username)
        .map { mLogs => mLogs.map(cmdaaLogMongo2Api) }
        .map { aLogs => Ok(Json toJson FetchLogList(aLogs)) }
    }

  /** Action for route, GET '/api/v1/logs/:logId' */
  def get(bsonLogId: BSONObjectID): Action[AnyContent] =
    authAction.async { authdReq =>
      val logId = bsonLogId.stringify
      val user = authdReq.user
      logger.info(s"GET 'logs/$logId' - User: ${user.username}")

      fetchCmdaaLog(bsonLogId)
    }

  /** Action for route: POST '/api/v1/logs' */
  def insertLog(): Action[JsValue] =
    authAction(parse.json).async { authdReq =>
      val user = authdReq.user
      logger.info(s"POST 'logs' - User: ${user.username}")

      authdReq.body.validate[InsertCmdaaLog]
        .fold(jsValidationResponse, insReq => {
          val cmdaaLog = newCmdaaLog2Mongo(user.username, insReq.cmdaaLog)
          val logId = cmdaaLog._id.stringify

          logDao.insert(cmdaaLog) flatMap  { _ =>
            logger.info(s"  logs/$logId - Inserted by ${user.username}")
            fetchCmdaaLog(cmdaaLog._id)
          }
        })
    }

  /** Action for route: PUT '/api/v1/logs/:logId/dsConfig' */
  def setDaemonSetConfig(bsonLogId: BSONObjectID): Action[JsValue] =
    authAction(parse.json).async { authdReq =>
      val logId = bsonLogId.stringify
      val user = authdReq.user
      logger.info(s"PUT 'logs/$logId/dsConfig' - User ${user.username}")

      authdReq.body.validate[SetDaemonSetConfig]
        .fold(jsValidationResponse, setDsConfig => {
          val dsConfig = setDaemonSetConfig2Mongo(setDsConfig)

          logDao
            .setDsConfig(bsonLogId, user.username, dsConfig)
            .map {
              case Some(cmdaaLog) =>
                logger.info(s"  PUT 'logs/$logId/dsConfig' - " +
                  s"Set by ${user.username}")
                res200Log(cmdaaLog)
              case None =>
                logger.warn(s"NOT_FOUND. SetLogLocation - " +
                  s"No suitable record found for LogId: '$logId'.")
                res404(logId)
            }
        })
    }

  /** Action for route: POST '/api/v1/logs/:logId/runs' */
  def insertGrokRun(bsonLogId: BSONObjectID): Action[JsValue] =
    authAction(parse.json).async { authdReq =>
      val logId = bsonLogId.stringify
      val user = authdReq.user
      logger.info(s"POST 'logs/$logId/runs' - User ${user.username}")

      authdReq.body.validate[InsertGrokRun]
        .fold(jsValidationResponse, insGrokReq => {
          val grokRun = newGrokRun2Mongo(insGrokReq.newGrokRun)

          logDao
            .insertGrokRun(bsonLogId, user.username, grokRun)
            .map {
              case Some(cmdaaLog) =>
                logger.info(s"  logs/$logId/runs/${grokRun._id} - " +
                  s"Inserted by ${user.username}")
                res200Log(cmdaaLog)
              case None =>
                logger.warn(s"NOT_FOUND. InsertGrokRun - " +
                  s"No suitable record found for LogId: '$logId'.")

                res404(s"$logId/ll=${grokRun.logLikelihood}," +
                  s"cos=${grokRun.cosSimilarity}")
            }
        })
    }

  /** Action for route: PUT '/api/v1/logs/:logId/runs/:runId/status' */
  def updateLogStatus (
    bsonLogId: BSONObjectID,
    bsonRunId: BSONObjectID
  ): Action[JsValue] = Action.async(parse.json) { req =>
    val logId = bsonLogId.stringify
    val runId = bsonRunId.stringify
    logger.info(s"PUT 'logs/$logId/runs/$runId/status'")

    req.body
      .validate[UpdateLogStatus]
      .fold(jsValidationResponse, updtReq => {
        logDao
          .updateRunStatus (
            bsonLogId, bsonRunId,
            runStatusApi2Mongo(updtReq.status)
          )
          .map {
            case Some(cmdaaLog) =>
              logger.info(s"  logs/$logId/runs/$runId/status - " +
                s"Status Set '${updtReq.status.message}'")
              res200Log(cmdaaLog)
            case None =>
              logger.warn("NOT_FOUND. UpdateLogStatus - " +
                s"No suitable record found for LogId/RunId: '$logId/$runId'.")
              res404(s"$logId/$runId")
          }
      })
  }

  /** Action for route: PUT '/api/v1/logs/:logId/runs/:runId/associatedEsInstance' */
  def updateEsInstance (
    bsonLogId: BSONObjectID,
    bsonRunId: BSONObjectID
  ): Action[JsValue] = Action.async(parse.json) { req =>
    val logId = bsonLogId.stringify
    val runId = bsonRunId.stringify
    logger.info(s"PUT 'logs/$logId/runs/$runId/esInstance'")

    req.body
      .validate[UpdateEsInstance]
      .fold(jsValidationResponse, updtReq => {
        logDao
          .updateEsInstance(bsonLogId, bsonRunId, updtReq.esInstance)
          .map {
            case Some(cmdaaLog) =>
              logger.info(s"  logs/$logId/runs/$runId/associatedEsInstance - " +
                s"EsInstance Set '${updtReq.esInstance}'")
              res200Log(cmdaaLog)
            case None =>
              logger.warn("NOT_FOUND. UpdateEsInstance - " +
                s"No suitable record found for LogId/RunId: '$logId/$runId'.")
              res404(s"$logId/$runId")
          }
      })
  }

  /** Action for route: DELETE '/api/v1/logs/:logId/runs/:runId/associatedEsInstance' */
  def deleteEsInstance (
    bsonLogId: BSONObjectID,
    bsonRunId: BSONObjectID
  ): Action[AnyContent] = Action.async(parse.anyContent) { _ =>
    val logId = bsonLogId.stringify
    val runId = bsonRunId.stringify
    logger.info(s"DELETE 'logs/$logId/runs/$runId/esInstance'")

    logDao
      .deleteEsInstance(bsonLogId, bsonRunId)
      .map {
        case Some(cmdaaLog) =>
          logger.info(s"  logs/$logId/runs/$runId/associatedEsInstance")
          res200Log(cmdaaLog)
        case None =>
          logger.warn("NOT_FOUND. DeleteEsInstance - " +
            s"No suitable record found for LogId/RunId: '$logId/$runId'.")
          res404(s"$logId/$runId")
      }
  }

  /** Action for route: PUT '/api/v1/logs/:logId/runs/:runId/dsDeployed' */
  def updateDaemonSetDeployed (
    bsonLogId: BSONObjectID,
    bsonRunId: BSONObjectID
  ): Action[JsValue] = Action.async(parse.json) { req =>
    val logId = bsonLogId.stringify
    val runId = bsonRunId.stringify
    logger.info(s"PUT 'logs/$logId/runs/$runId/dsDeployed'")

    req.body
      .validate[UpdateDaemonSetDeployed]
      .fold(jsValidationResponse, updtReq => {
        logDao
          .updateDaemonSetDeployed(bsonLogId, bsonRunId, updtReq.dsDeployed)
          .map {
            case Some(cmdaaLog) =>
              logger.info(s"  logs/$logId/runs/$runId/dsDeployed - " +
                s"dsDeployed Set to '${updtReq.dsDeployed}'")
              res200Log(cmdaaLog)
            case None =>
              logger.warn("NOT_FOUND. UpdateLogStatus - " +
                s"No suitable record found for LogId/RunId: '$logId/$runId'.")
              res404(s"$logId/$runId")
          }
      })
  }

  /** Action for route: PUT '/api/v1/logs/:logId/runs/:runId/groks/:idx' */
  def updateGrok(bsonLogId: BSONObjectID, bsonRunId: BSONObjectID, idx: Int)
  : Action[JsValue] = Action.async(parse.json) { req =>
    val logId = bsonLogId.stringify
    val runId = bsonRunId.stringify
    logger.info(s"PUT 'logs/$logId/runs/$runId/groks/$idx")

    req.body.validate[UpdateGrok]
      .fold(jsValidationResponse, updtReq => {
        logDao
          .updateGrok(bsonLogId, bsonRunId, idx, updtReq.grok)
          .map {
            case Some(cmdaaLog) =>
              logger.info (
                s"  logs/$logId/runs/$runId/groks/$idx - "
                  + s"Grok Updated '${updtReq.grok}'"
              )
              res200Log(cmdaaLog)
            case None =>
              logger.warn(
                "NOT_FOUND. UpdateGrok - No suitable record found for "
                  + s"LogId/RunId/idx: '$logId/$runId/$idx'."
              )
              res404(s"$logId/$runId/$idx")
          }
      })
  }

  /** Action for route: PUT '/api/v1/logs/:logId/runs/:runId/groks' */
  def setGroks(bsonLogId: BSONObjectID, bsonRunId: BSONObjectID)
  : Action[JsValue] = Action.async(parse.json) { req =>
    val logId = bsonLogId.stringify
    val runId = bsonRunId.stringify
    logger.info(s"PUT 'logs/$logId/runs/$runId/groks'")

    req.body.validate[SetGroks]
      .fold(jsValidationResponse, setGroksReq => {
        logDao
          .setGroks (
            bsonLogId, bsonRunId,
            setGroksReq.groks.map(grokApi2Mongo)
          )
          .map {
            case Some(cmdaaLog) =>
              logger.info(s"  logs/$logId/runs/$runId/groks - Groks Set.")
              res200Log(cmdaaLog)
            case None =>
              logger.warn(
                "NOT_FOUND. SetGroks - No suitable record found for " +
                  s"LogId/RunId: '$logId/$runId'.")
              res404(s"$logId/$runId")
          }
      })
  }

  /** Action for route: POST '/api/v1/logs/:logId/runs/:runId/groks' */
  def addGroks2Set(bsonLogId: BSONObjectID, bsonRunId: BSONObjectID)
  : Action[JsValue] = Action.async(parse.json) { req =>
    val logId = bsonLogId.stringify
    val runId = bsonRunId.stringify

    logger.info(s"PUT 'logs/$logId/runs/$runId/groks'")

    req.body.validate[SetGroks]
      .fold(jsValidationResponse, addGroksReq => {
        logDao.addToGrokSet(
          bsonLogId, bsonRunId,
          addGroksReq.groks.map(grokApi2Mongo)
        )
        .map {
          case Some(cmdaaLog) =>
            logger.info(s"  logs/$logId/runs/$runId/groks - Groks Added.")
            res200Log(cmdaaLog)
          case None =>
            logger.warn(
              "NOT_FOUND. SetGroks - No suitable record found for " +
                s"LogId/RunId: '$logId/$runId'."
            )
            res404(s"$logId/$runId")
        }
      })
  }

  /** Action for route: DELETE '/api/v1/logs/:id' */
  def deleteLog(bsonLogId: BSONObjectID): Action[AnyContent] =
    authAction(parse.anyContent).async { authdReq =>
      val logId = bsonLogId.stringify
      val user = authdReq.user
      logger.info(s"DELETE 'logs/$logId' - User ${user.username}")

      logDao
        .delete(bsonLogId)
        .map { wr =>
          if (wr.n != 0) {
            logger.info(s"  logs/$logId - Deleted by ${user.username}")
            Ok(Json.obj("id" -> logId, "message" -> "deleted"))
          } else {
            logger.warn("NOT_FOUND. DeleteLog - " +
              s"No records removed for Log: '$logId'")
            res404(logId)
          }
        }
    }

  /** Action for route: DELETE 'api/v1/logs/:logId/runs/:runId' */
  def deleteRun(bsonLogId: BSONObjectID, bsonRunId: BSONObjectID)
  : Action[AnyContent] = authAction(parse.anyContent).async { authdReq =>
    val logId = bsonLogId.stringify
    val runId = bsonRunId.stringify
    val user = authdReq.user
    logger.info(s"DELETE 'logs/$logId/runs/$runId' - User ${user.username}")

    logDao
      .deleteRun(bsonLogId, bsonRunId, user.username)
      .map {
        case Some(cmdaaLog) =>
          logger.warn(s"  logs/$logId/runs/$runId - Deleted by ${user.username}")
          res200Log(cmdaaLog)
        case None =>
          logger.info(s"NOT_FOUND. DeleteRun - No suitable record found.")
          res404(s"$logId/$runId")
      }
  }

  /**
   * Fetches a CmdaaLog by ID and marshals it into a Result
   *
   * @param bsonLogId the logId to search for the CmdaaLog by
   * @param ec    ExecutionContext
   * @return the Result for attempting to fetch the CmdaaLog
   */
  private def fetchCmdaaLog(bsonLogId: BSONObjectID)
    (implicit ec: ExecutionContext)
  : Future[Result] = {
    val logId = bsonLogId.stringify

    logDao.findByLogId(bsonLogId) map { o_cmdaaLog =>
      o_cmdaaLog.fold {
        logger.warn(s"NOT_FOUND. Collection '$colName'; LogId '$logId'")
        res404(logId)
      } { cmdaaLog =>
        logger.info(s"Collection '$colName': GET CmdaaLog '$logId'")
        res200Log(cmdaaLog)
      }
    }
  }

  /** Generic JSON Validation Error Response */
  private def jsValidationResponse (
    errors: scSeq[(JsPath, scSeq[JsonValidationError])]
  ): Future[Result] = Future.successful {
    BadRequest(Json.obj("message" -> JsError.toJson(errors)))
  }

  /** Respond with a CmdaaLog */
  private def res200Log(cmdaaLog: mongo.CmdaaLog): Result =
    Ok(Json toJson FetchCmdaaLog(cmdaaLogMongo2Api(cmdaaLog)))

  /** Generic Response for a document can not be found by ID */
  private def res404(id: String): Result =
    NotFound(Json.obj("status" -> "NotFound", "code" -> 404, "id" -> id))
}
