/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.cmdaa.gs.service

import com.typesafe.config.Config
import play.api.ConfigLoader

/**
 * Application Configuration model.
 *
 * @param authServiceUrl the base URL of the Authentication Service.
 * @param bucket         the S3Bucket being used
 */
case class AppConfig (
  authServiceUrl: String,
  bucket:         String
)
object AppConfig {

  /** The required "hook" for integrating with Play's Configuration */
  implicit val configLoader: ConfigLoader[AppConfig] =
    (rootConfig: Config, path: String) => {
      val config = rootConfig.getConfig(path)

      AppConfig (
        config.getString("authServiceUrl"),
        config.getString("bucket")
      )
    }
}
