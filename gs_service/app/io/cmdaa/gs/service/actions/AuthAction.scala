/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.cmdaa.gs.service.actions

import io.cmdaa.auth.api.CmdaaUser
import io.cmdaa.gs.service.dao.AuthUserDao
import play.api.http.HeaderNames
import play.api.mvc.Results.Unauthorized
import play.api.mvc._

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

/**
 * Helper Action for specifying that Authentication is required for an API
 * endpoint and providing a WrappedRequest object containing the
 * Authenticated User.
 *
 * @param parser           default BodyParsers
 * @param authUserDao      Authenticated User Data Access Object
 * @param executionContext execution context for async activities
 */
class AuthAction @Inject() (
  val parser: BodyParsers.Default,
  val authUserDao: AuthUserDao
)(implicit
  protected val executionContext: ExecutionContext
) extends ActionBuilder[AuthdRequest, AnyContent] {
  import AuthAction.r_headerToken

  /**
   * Wraps the given block, augmenting its Request with the Authenticated user.
   * If a user could not be authenticated, an Unauthorized response is given.
   *
   * @param request the original request
   * @param block   the body of the Action
   * @tparam A the ContentType of the Request
   * @return the Asynchronous Result
   */
  override def invokeBlock[A] (
    request: Request[A],
    block: AuthdRequest[A] => Future[Result]
  ): Future[Result] = {
    extractBearerToken(request) match {
      case Some(token) =>
        authUserDao.fetchUserByToken(token) flatMap { o_user =>
          o_user map { user =>
            block(new AuthdRequest(user, request))
          } getOrElse { // No user found with provided AuthToken
            Future successful Unauthorized
          }
        }

      case None => // No AuthToken presented
        Future successful Unauthorized
    }
  }

  /**
   * Extracts the bearer token from the Request.
   *
   * @param request the incoming request
   * @tparam A the ContentType of the request
   * @return the Bearer Token, if any
   */
  private def extractBearerToken[A](request: Request[A])
  : Option[String] =
    request.headers.get(HeaderNames.AUTHORIZATION) collect {
      case r_headerToken(token) => token
    }
}
object AuthAction {
  /** Regex for parsing Bearer Tokens from the "Authorization" Header */
  private val r_headerToken = """Bearer (.+?)""".r
}

/**
 * A WrappedRequest containing the authenticated user.
 *
 * @param user    the authenticated user accessing this service
 * @param request the request being wrapped
 * @tparam A the ContentType of the Request
 */
class AuthdRequest[A] (
  val user: CmdaaUser,
  request: Request[A]
) extends WrappedRequest[A](request)
