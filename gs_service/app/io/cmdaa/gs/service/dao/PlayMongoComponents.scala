/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.cmdaa.gs.service.dao

import io.cmdaa.gs.dao.MongoComponents
import play.modules.reactivemongo.ReactiveMongoApi
import reactivemongo.api.{AsyncDriver, DB, MongoConnection}

import javax.inject.Inject
import scala.concurrent.Future

/**
 * Play Implementation of MongoComponents which delegates to the
 * ReactiveMongoApi from the play2-reactivemongo plugin.
 *
 * @param reactiveMongoApi the ReactiveMongoApi to delegate to.
 */
class PlayMongoComponents @Inject() (
  private val reactiveMongoApi: ReactiveMongoApi
) extends MongoComponents {
  override def asyncDriver: AsyncDriver     = reactiveMongoApi.asyncDriver
  override def connection:  MongoConnection = reactiveMongoApi.connection
  override def database:    Future[DB]      = reactiveMongoApi.database
}
