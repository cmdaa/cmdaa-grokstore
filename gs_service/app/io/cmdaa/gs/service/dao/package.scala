/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.cmdaa.gs.service

import akka.stream.alpakka.s3.ListBucketResultContents
import io.cmdaa.gs.api.req.{NewCmdaaLog, NewGrokRun, SetDaemonSetConfig}
import io.cmdaa.gs.api.s3.BucketItem
import io.cmdaa.gs.{api, model => mongo}
import reactivemongo.api.bson._

import java.text.SimpleDateFormat
import java.util.Date

/**
 * This package differs from the gs_dao project in that it exists to supply
 * utilities for using gs_dao from the service. It also has "DAOs" for
 * interacting with other Web Services.
 */
package object dao {

  /**
   * Transforms NewCmdaaLog request data into a mongo.CmdaaLog
   *
   * @param owner  the username of the owner requesting a new CmdaaLog
   * @param newLog the data for the creation of the requested CmdaaLog
   * @return BSON representation of the CmdaaLog
   */
  def newCmdaaLog2Mongo(owner: String, newLog: NewCmdaaLog)
  : mongo.CmdaaLog = {
    mongo.CmdaaLog (
      mongo.DocMeta.fromUser(owner),
      owner,
      newLog.bucket,
      newLog.path,
      Some(newLog.filename),
      mongo.GrokRun (
        logLikelihood = newLog.llThreshold,
        cosSimilarity = newLog.cosThreshold
      ):: Nil,
    )
  }

  /**
   * Transforms NewGrokRun request data into a mongo.GrokRun.
   *
   * @param newGrokRun the data for the creation of a new GrokRun
   * @return BSON representation of the new GrokRun
   */
  def newGrokRun2Mongo(newGrokRun: NewGrokRun): mongo.GrokRun =
    mongo.GrokRun (
      logLikelihood = newGrokRun.llThreshold,
      cosSimilarity = newGrokRun.cosThreshold
    )

  /** Transforms SetLogLocation request data into a mongo.LogLocation */
  def setDaemonSetConfig2Mongo(setDsConfig: SetDaemonSetConfig)
  : mongo.DsConfig =
    mongo.DsConfig (
      paths    = strArr2bsonArr(setDsConfig.dsConfig.paths),
      nodeLbls = strArr2bsonArr(setDsConfig.dsConfig.nodeLbls),
      setDsConfig.dsConfig.daemonPrefix
    )

  /**
   * Mongo to API conversion for CmdaaLog
   *
   * @param mLog mongo CmdaaLog
   * @return the api.CmdaaLog equivalent
   */
  def cmdaaLogMongo2Api(mLog: mongo.CmdaaLog): api.CmdaaLog =
    api.CmdaaLog (
      mLog._id.stringify,
      docMetaMongo2Api(mLog.meta),
      mLog.owner,
      mLog.bucket,
      mLog.path,
      mLog.filename,
      mLog.runs.map(grokRunMongo2Api),
      mLog.deployGroks,
      mLog.dsConfig.map(dsConfigMongo2Api)
    )

  /**
   * Mongo to API conversion for GrokRun.
   *
   * @param mRun mongo GrokRun
   * @return the api.GrokRun equivalent
   */
  def grokRunMongo2Api(mRun: mongo.GrokRun): api.GrokRun =
    api.GrokRun (
      mRun._id.stringify,
      bdt2dts(mRun.submitted),
      runStatusMongo2Api(mRun.status),
      mRun.logLikelihood,
      mRun.cosSimilarity,
      mRun.associatedEsInstance,
      mRun.dsDeployed.getOrElse(false),
      mRun.groks.map(grokMongo2Api)
    )

  /** Mongo to API conversion for DsConfig */
  def dsConfigMongo2Api(mLoc: mongo.DsConfig): api.DsConfig =
    api.DsConfig (
      mLoc.paths   .values.flatMap(_.asOpt[String]).toArray,
      mLoc.nodeLbls.values.flatMap(_.asOpt[String]).toArray,
      mLoc.daemonPrefix
    )

  /**
   * Mongo to API conversion for RunStatus.
   *
   * @param mRunStatus mongo RunStatus
   * @return an API RunStatus
   */
  def runStatusMongo2Api(mRunStatus: mongo.RunStatus): api.RunStatus =
    api.RunStatus (
      mRunStatus.isRunning,
      mRunStatus.isComplete,
      mRunStatus.isError,
      mRunStatus.message
    )

  /**
   * API to Mongo conversion for RunStatus
   *
   * @param aRunStatus api RunStatus
   * @return mongo RunStatus
   */
  def runStatusApi2Mongo(aRunStatus: api.RunStatus): mongo.RunStatus =
    mongo.RunStatus (
      aRunStatus.isRunning,
      aRunStatus.isComplete,
      aRunStatus.isError,
      aRunStatus.message
    )

  /**
   * Mongo to API conversion for Grok
   *
   * @param mGrok mongo Grok
   * @return the api.Grok equivalent
   */
  def grokMongo2Api(mGrok: mongo.Grok): api.Grok =
    api.Grok(mGrok.grok, mGrok.logs)

  /**
   * API to Mongo conversion for Grok
   *
   * @param aGrok api Grok
   * @return the mongo.Grok equivalent
   */
  def grokApi2Mongo(aGrok: api.Grok): mongo.Grok =
    mongo.Grok(aGrok.grok, aGrok.logs)

  /**
   * Mongo to API conversion for DocMeta
   *
   * @param mMeta mongo DocMeta
   * @return the api.DocMeta equivalent
   */
  def docMetaMongo2Api(mMeta: mongo.DocMeta): api.DocMeta =
    api.DocMeta (
      mMeta.createdBy,
      mMeta.updatedBy,
      bdt2dts(mMeta.created),
      bdt2dts(mMeta.updated),
      mMeta.schema
    )

  /** Converts a S3 "BucketItem" into an API BucketItem */
  def s3BucketItem2Api(c: ListBucketResultContents): BucketItem =
    BucketItem (
      bucket   = c.bucketName,
      path     = c.key,
      size     = c.size,
      modified = c.lastModified.toEpochMilli
    )

  /**
   * Java DateFormatter isn't thread safe; this creates a preconfigued
   * DateFormatter as needed.
   *
   * @return a preconfigued DateFormatter
   */
  private def dateFormatter =
    new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")

  /**
   * Converts a BSONDateTime to ISO 8601 version of the W3C XML Schema
   * DateTime.
   *
   * @param bdt BSON DateTime
   * @return a standards formatted String
   */
  private def bdt2dts(bdt: BSONDateTime): String =
    dateFormatter.format(new Date(bdt.value))

  private def strArr2bsonArr(strArr: Array[String]): BSONArray =
    BSONArray(strArr.map(BSONString.apply).toIndexedSeq)
}
