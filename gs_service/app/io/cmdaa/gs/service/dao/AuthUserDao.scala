/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.cmdaa.gs.service.dao

import io.cmdaa.auth.api.CmdaaUser
import io.cmdaa.auth.api.resp.CmdaaUserResp
import io.cmdaa.gs.service.AppConfig
import play.api.Configuration
import play.api.libs.json.Json
import play.api.libs.ws.WSClient

import javax.inject.{Inject, Singleton}
import scala.concurrent.{ExecutionContext, Future}

/**
 * Data Access Object for retrieving authorized users from auth_session_svc.
 *
 * @param config Play Configuration
 * @param ws the WebService Client
 * @param ec the execution context
 */
@Singleton
class AuthUserDao @Inject() (
  config: Configuration,
  ws:     WSClient
)(implicit ec: ExecutionContext) {
  /** Base URL for the AuthSessionService */
  private val authSvcUrl = config.get[AppConfig]("app").authServiceUrl

  /**
   * Fetches a CmdaaUser by token from the AuthSessionService.
   *
   * @param token the Auth Bearer Token from the request
   * @return async, optional CmdaaUser
   */
  def fetchUserByToken(token: String): Future[Option[CmdaaUser]] = {
    val f_response = ws
      .url(s"$authSvcUrl/api/v1/auth")
      .withHttpHeaders("authorization" -> s"Bearer $token")
      .get()

    f_response map { resp =>
      resp.status match {
        case 200 =>
          // TODO: Handle JSON formatting errors better.
          Json.fromJson[CmdaaUserResp](resp.json)
            .asOpt.map(_.user)
        case _ =>
          None
      }
    }
  }
}
