FROM openjdk:11-jre

WORKDIR /svc
COPY gs_service/target/universal/*.zip .

RUN unzip *.zip && mv cmdaa*/* . && rm -rf cmdaa*/ *.zip

ENV MONGODB_URL="mongodb://cmdaa:cmdaa@mongodb:27017/cmdaa" \
    AUTH_SVC_URL="http://cmdaa-auth" \
    S3_REGION="us-east-1" \
    MINIO_ENDPOINT="http://minio:80" \
    MINIO_KEY="cmdaa" \
    MINIO_SECRET="cmdaacmdaa" \
    MINIO_BUCKET="cmdaa"

EXPOSE 80
ENTRYPOINT /svc/bin/cmdaa-gs-service -Dhttp.port=80
