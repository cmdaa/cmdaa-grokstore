#!/bin/bash

TAG=0.0.23

sbt dist

eval $(minikube docker-env)
docker build -t cmdaa/cmdaa-grokstore:${TAG} .
#docker push     cmdaa/cmdaa-grokstore:${TAG}

kubectl -n cmdaa rollout restart deploy cmdaa-grokstore
exec kubectl -n cmdaa get pods --watch
